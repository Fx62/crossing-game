var canvas = document.getElementById('workSpace');
var ctx = canvas.getContext('2d');

let screenWidth = canvas.width;
let screenHeight = canvas.height;

var liveGame = true;
var isLeft = false;
var isRight = false;

class GameCharacter {
    constructor(x, y, widht, height, color, speed) {
        this.x = x;
        this.y = y;
        this.widht = widht;
        this.height = height;
        this.color = color;
        this.speed = speed;
    }

    moveVertically() {
        if (this.y > screenHeight - 50 || this.y < 0) {
            this.speed = -this.speed;
        }
        this.y += this.speed;
    }

    moveHorizontally() {
        if (this.x > screenWidth - 100 || this.x < 0) {
            this.speed = -this.speed;
        }
        this.x += this.speed;
    }
}

//var rectangle = new GameCharacter(50, 50, 50, 50, "#8B0000");
var player = new GameCharacter(50, screenHeight / 2 - 50, 50, 50, "#483D8B", 0);

var enemies = [
    new GameCharacter(0 + 150, 0 + 50, 50, 50, "#8B0000", 2),
    new GameCharacter(400, screenHeight - 100, 50, 50, "#8B0000", 3),
    new GameCharacter(620, 0 + 100, 50, 50, "#8B0000", 3),
    new GameCharacter(screenWidth - 150, 0 + 50, 50, 50, "#8B0000", 4)
]

var goal = new GameCharacter(screenWidth - 50, screenHeight / 2 - 50, 50, 100, "#00BFFF", 0);

var collition = function (sqr1, sqr2) {
    let sqr1x2 = sqr1.x + sqr1.widht;
    let sqr2x2 = sqr2.x + sqr2.widht;
    let sqr1y2 = sqr1.y + sqr1.height;
    let sqr2y2 = sqr2.y + sqr2.height;
    return sqr1.x < sqr2x2 && sqr1x2 > sqr2.x && sqr1.y < sqr2y2 && sqr1y2 > sqr2.y;
}

var endGameLogic = function (text) {
    liveGame = false;
    alert(text);
    window.location = "";
}

var sprites = {}

var loadSprites = function(){
    sprites.player = new Image();
    sprites.player.src = 'images/hero.png';

    sprites.background = new Image();
    sprites.background.src = 'images/floor.png';

    sprites.enemy = new Image();
    sprites.enemy.src = 'images/enemy.png';

    sprites.goal = new Image();
    sprites.goal.src = 'images/chest.png';
}

var update = function () {
    if (collition(player, goal)) {
        endGameLogic("You win!")
    }
    player.moveHorizontally();
    enemies.forEach(function (element) {
        if (collition(player, element)) {
            endGameLogic("Game Over");
        }
        element.moveVertically();
    });
}

document.onkeydown = function (event) {
    switch (event.keyCode) {
        case 39:
            isRight = true;
            player.speed = 4;
            break;
        case 37:
            isLeft = true;
            player.speed = -4;
            break;
    }
}

document.onkeyup = function (event) {
    switch(event.keyCode) {
        case 39:
            isRight = false;
            if(isLeft){
                player.speed = -player.speed;
            } else{
                player.speed = 0;
            }
            break;
            case 37:
                isLeft = false;
                if(isRight){
                    player.speed = player.speed;
                } else{
                    player.speed = 0;
                }
                break;
    }
    //player.speed = 0;
}

var draw = function () {
    ctx.clearRect(0, 0, screenWidth, screenHeight);
    //ctx.fillStyle = player.color;
    //ctx.fillRect(player.x, player.y, player.widht, player.height);
    ctx.drawImage(sprites.background, 0, 0);
    ctx.drawImage(sprites.player, player.x, player.y);
    ctx.drawImage(sprites.goal, goal.x, goal.y);
    /*enemies.forEach(function (element) {
        ctx.fillStyle = element.color;
        ctx.fillRect(element.x, element.y, element.widht, element.height);
    });*/
    enemies.forEach(function(element){
        ctx.drawImage(sprites.enemy, element.x, element.y);
    });
    //ctx.fillStyle = goal.color;
    //ctx.fillRect(goal.x, goal.y, goal.widht, goal.height);
}

var step = function () {
    update();
    draw();
    if (liveGame) {
        window.requestAnimationFrame(step);
    }
}

loadSprites();
step();